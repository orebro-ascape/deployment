# ASCAPE Local Deployment 

The ASCAPE Local Deployment repository is an open-access repository that pertains to ASCAPE software. It contains instructions, scripts and yaml configuration files for the deployment
of the parts of the ASCAPE software that are meant to be deployed locally inside a healthcare
provider's IT infrastructure.

Though ad hoc installation scenarios are of course possible, the instructions herein assume the standard installation scenario whereby the local ASCAPE software is configured so that it interoperates with the ASCAPE Cloud and takes advantage of the common ASCAPE Infrastructure.


## Contents 

Two folders can be found here.

| File | Description |
| - | - |
|scripts | A folder with scripts facilitating the deployment of the ASCAPE local software on a kubernetes cluster |
|yaml | A folder with yaml configuration files |

## Instalation Instructions


1. Clone/download the repo. 
2. Visit the `secrets` repo and follow instructions given there.
3. Change directory to the `deployment` directory
4. Edit the `ascape-apply` script in the `scripts` folder, in order to configure your installation.  See the next section for the available configuration options.
5. Review the resource requests and limits in the yaml files (in the `yaml` directory)
5. Run the 'scripts/ascape-install' script (e.g. `source scripts/ascape-install`)
6. This will initialise the edge node, transformation and dashboard components and set them up appropriately.
7. Wait for all pods to initialise before proceeding to use the ASCAPE local software installation.  This may take anywhere from a few minutes to two hours depending on the resource specifications.
8. Take appropriate measures to securely expose the ASCAPE Dashboard in your network (kubernets service `ascape-ui/dashboard`).




## Configuration options

| File | Possible Values | Description |
| - | - |- |
|`ASCAPE_ENV` | `testing`/`prodcution`| Indicates whether your ASCAPE edge node will be connected to the testing or the production ASCAPE Cloud (should be `production` once everything is tested and `testing` before that) |
|`PARTICIPATES_IN_FL` |`yes`/`no`| Indicates whether your ASCAPE edge node will participate in federated learning (should be `yes` once everything is tested and `no` before that) |
|`PARTICIPATES_IN_HL`| `yes`/`no` | Indicates whether your ASCAPE edge node will participate in homomorphic learning (should be `yes` once everything is tested and `no` before that) |



## Update Instructions

You may  re-run `scripts/ascape-install` to update all components or execute one of the following scripts to update the corresponding set of local components: 
`scripts/ascape-update-edge`,`scripts/ascape-update-transformation`,`scripts/ascape-update-dashboard`.